import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ant from "./utils/ant";
import "@/style/ant.scss";
// 导航守卫
import "./utils/permission";
import { message } from 'ant-design-vue'
Vue.prototype.$message = message;

Vue.use(ant);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
