import {
  Input,
  Button,
  Menu,
  Icon,
  Card,
  Popover,
  Table,
  Tree,
  Form,
  Select

} from "ant-design-vue";
const ant = {
  install(Vue) {
    Vue.component(Button.name, Button);
    Vue.component(Input.name, Input);
    Vue.component(Card.name, Card);
    Vue.component(Popover.name, Popover);
    Vue.component(Table.name, Table);
    Vue.use(Select);
    Vue.use(Menu);
    Vue.use(Tree);
    Vue.use(Form);
    Vue.component(Icon.name, Icon);
  },
};
export default ant;
