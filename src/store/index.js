import Vue from "vue";
import Vuex from "vuex";
import createLoadingPlugin from "utils/vuex-loading";

Vue.use(Vuex);

const files = require.context("./modules", false, /\.js$/);
const modules = {};
files.keys().forEach(key => {
  modules[key.replace(/(\.\/|\.js)/g, "")] = files(key).default;
});
export default new Vuex.Store({
  plugins: [createLoadingPlugin()],
  state: {
    direction: "forward", // 页面切换方向
    loading: true,
    page: 0,
    open: 100,
    show: true,
    time: Number(new Date()),
    userId: 2,
  },
  getters: {
    userData(state) {
      return state.user.user;
      // return getters['user/user']
    },
    getOpen(state) {
      return function (val) {
        return val + state.open;
      };
    },
  },
  mutations: {
    // 更新页面切换方向
    updateDirection(state, direction) {
      state.direction = direction;
    },
    // 修改page 参数
    pageChange(state, page) {
      state.page = page;
    },
    setOpen(state, val = 1) {
      state.open = state.open + val;
    },
    setTimes(state, time) {
      state.time = time;
    },
  },
  actions: {
    setOpens({ commit }, val) {
      setTimeout(() => {
        commit("setOpen", val);
      }, 1000);
    },
  },
  modules,
});
