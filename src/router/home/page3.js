export default [
  {
    path: "/page3",
    name: "table",
    component: () => import("../../views/home/page3.vue"),
    meta: {
      auth: true,
      title: "table",
      keepAlive: true,
      iocn: "bank"

    },
  }
];
