export default [
  {
    path: "/page1",
    name: "生命周期",
    component: () => import("../../views/home/page/mapCesium.vue"),
    meta: {
      auth: true,
      title: "页面1",
      keepAlive: true,
      iocn: "book"
    },
  }
];
