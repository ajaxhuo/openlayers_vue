export default [
  {
    path: "/mapImg",
    name: "自定义地图",
    component: () => import("../../views/home/mapImg.vue"),
    meta: {
      auth: false,
      title: "日志",
      keepAlive: false,
      iocn: "credit-card"

    },
  }
];
