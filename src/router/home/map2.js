export default [
  {
    path: "/map2JSON",
    name: "JSON2地图",
    component: () => import("../../views/home/map2JSON.vue"),
    meta: {
      auth: false,
      title: "围栏",
      keepAlive: false,
      iocn: "credit-card"

    },
  }
];
