export default [
  {
    path: "/home",
    name: "Home",
    component: () => import("../../views/home/page/mapTest.vue"),

    meta: {
      auth: true,
      title: "首页",
      keepAlive: true,
      iocn: "mail"
    },
    // children: [
    //   {
    //     path: "/home/computed",
    //     name: "openlayers",
    //     component: () => import("views/home/page/test1.vue"),
    //     meta: {
    //       auth: true,
    //       title: "openlayers",
    //       keepAlive: true,
    //       iocn: "calendar"
    //     },
    //   },
    //   {
    //     path: "/home/3d",
    //     name: "mopbox",
    //     component: () => import("views/home/page/3d.vue"),
    //     meta: {
    //       auth: true,
    //       title: "首页-computed",
    //       keepAlive: true,
    //       iocn: "calendar"
    //     },
    //   },
    //   {
    //     path: "/home/change",
    //     name: "自定义地图",
    //     component: () => import("views/home/page/mapTest.vue"),
    //     meta: {
    //       auth: true,
    //       title: "首页-自定义地图",
    //       keepAlive: true,
    //       iocn: "calendar"
    //     },
    //   },
    //   // ol-cesium
    //   {
    //     path: "/home/cesium",
    //     name: "Home-cesium",
    //     component: () => import("views/home/page/mapCesium.vue"),
    //     meta: {
    //       auth: true,
    //       title: "首页-cesium",
    //       keepAlive: true,
    //       iocn: "calendar"
    //     },
    //   }
    // ],
  }
];
