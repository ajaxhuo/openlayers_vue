import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
// import Vue from "vue";
const loadRoutes = files =>
  files
    .keys()
    .reduce((arr, key) => {
      const routes = files(key).default;
      return typeof routes === "object" ? arr.concat(routes) : arr;
    }, [])
    .sort((prev, next) => (prev.sort || 0) - (next.sort || 0));
const children = loadRoutes(require.context("./home", false, /\.js$/));
localStorage.setItem("router", JSON.stringify(children));
let routes = [
  {
    path: "/",
    name: "home",
    meta: {
      title: "首页",
      keepAlive: true,
    },
    redirect: "/home",
    component: () => import("../views/Home.vue"),
    children,
  },
  {
    path: "/404",
    name: "404",
    component: () => import("views/404.vue"),
    meta: {
      title: "404",
      keepAlive: true,
    },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("views/user/login.vue"),
    meta: {
      title: "登陆",
      keepAlive: false,
    },
  }
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    // 配置url 默认添加项 process.env.BASE_URL
    base: process.env.BASE_URL,
    routes,
  });
const router = createRouter();
export default router;
// export function resetRouter() {
//   myRouter.replace({
//     name: "login",
//   });
// }
