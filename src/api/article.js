import request from "@/utils/request";

// 文章列表
export function article() {
  return request({
    url: "/profile ",
    method: "get",
    showLoading: false
  });
}
// 地图列表
export function map(url = "/map",) {
  return request({
    url: url,
    method: "get",
    showLoading: false
  });
}
// 新增地图图层  修改地图
export function hole(data, url = "/map/hole", method = "post") {
  return request({
    url,
    method,
    data,
    showLoading: false
  });
}
// 删除地图图层

// 发送数据updatePoint
export function updatePoint(data, url = "/updatePoint",) {
  return request({
    url: url,
    method: "post",
    data,
    showLoading: false
  });
}
// getPoint
export function getPoint(url = "/getPoint",) {
  return request({
    url: url,
    method: "get",
    showLoading: false
  });
}